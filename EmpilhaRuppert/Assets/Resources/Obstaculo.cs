﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaculo : MonoBehaviour {

    public float speed;

    private GameManager _gameManager;

	// Use this for initialization
	void Start ()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        Destroy(this.gameObject, 6f);
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Vector3.down * _gameManager.velocidade * Time.deltaTime);

	}    
}
