﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveGround : MonoBehaviour {

    private float _speed;

	// Use this for initialization
	void Start ()
    {
        _speed = 4.0f;	
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
        if (transform.position.y < -20)
        {
            Destroy(this.gameObject);
        }
	}
}
