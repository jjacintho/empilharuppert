﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour {

    public Button iniciar;
    public Button sair;
    public Button som;

    public Sprite somON;
    public Sprite somOFF;

	// Use this for initialization
	void Start () {
        iniciar.onClick.AddListener(delegate { presIniciar(); });
        sair.onClick.AddListener(delegate { presSair(); });
        som.onClick.AddListener(delegate { presSom(); });
	}
	
	// Update is called once per frame
	void Update () {
        attSom();
	}

    public void presIniciar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        if(Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
    }

    public void presSair()
    {
        Application.Quit();
    }

    public void presSom()
    {
        if (som.image.sprite == somON)
        {
            som.image.sprite = somOFF;
            AudioListener.pause = true;
        } else
        {
            som.image.sprite = somON;
            AudioListener.pause = false;
        }
    }

    public void attSom()
    {
        if (AudioListener.pause == false)
        {
            som.image.sprite = somON;
        }
        else
        {
            som.image.sprite = somOFF;
        }
    }
}
