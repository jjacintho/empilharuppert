﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public bool gameOver;
    public bool delay;

    public float chaoSpawnTime;
    public float obsSpawnTime;

    public int pontosParaDistribuir;
    public float forca;
    public float velocidade;

    private Player _player;
    private Caixa _caixa;
    private Obstaculo _obstaculo;
    private MoveGround _moveGround;
    private SpawnManager _spawnManager;
    private VidaManager _vidaManager;

    [SerializeField]
    private GameObject _finalFasePrefab;
    [SerializeField]
    private GameObject _caminhos;
    [SerializeField]
    private GameObject _paredes;
    [SerializeField]
    private GameObject _playerPrefab;
    [SerializeField]
    private GameObject _caixaPrefab;
    

    void Start()
    {
        gameOver = false;
        delay = false;

        pontosParaDistribuir = 0;
        forca = 0;
        velocidade = 5;
        obsSpawnTime = 4.0f;
        chaoSpawnTime = 3.5f;

        _player = GameObject.Find("Player").GetComponent<Player>();
        _caixa = GameObject.Find("Caixa").GetComponent<Caixa>();
        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        _vidaManager = GameObject.Find("Canvas").GetComponent<VidaManager>();

        StartCoroutine(GameTimeRoutine());
    }

    IEnumerator GameTimeRoutine()
    {
        yield return new WaitForSeconds(60.0f);
        StartCoroutine(DelayRoutine());
        gameOver = true;
        pontosParaDistribuir += 1;
        _vidaManager.AtualizarPontosParaDistribuir();
        _playerPrefab.transform.position = new Vector3(-0.1f, -7.0f, 0.0f);
        _caixaPrefab.transform.position = new Vector3(-0.1f, -5.5f, 0.0f);
        Instantiate(_finalFasePrefab, new Vector3(0, 20, 0), Quaternion.identity);
    }

    IEnumerator DelayRoutine()
    {
        yield return new WaitForSeconds(3.0f);
        delay = true;
    }

    public void Reiniciar()
    {
        //GameObject finalTextil = GameObject.Find("FinalTextil(Clone)");
        //Destroy(finalTextil);
        Instantiate(_caminhos, new Vector3(0, 0, 0), Quaternion.identity);
        Instantiate(_paredes, new Vector3(0, 0, 0), Quaternion.identity);
        _playerPrefab.transform.position = new Vector3(-0.1f, -7.0f, 0.0f);
        _caixaPrefab.transform.position = new Vector3(-0.1f, -5.5f, 0.0f);
        gameOver = false;
        delay = false;
        _caixa.mudarVelocidadeAnimacao();
        _spawnManager.StartCoroutines();
        _vidaManager.EsconderTelaFinal();
        _vidaManager.EsconderGameOver();
        _vidaManager.UpdateVida(3);
        StartCoroutine(GameTimeRoutine());
        Time.timeScale = 1;
    }

}
