﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VidaManager : MonoBehaviour {

    [SerializeField]
    private Sprite newCaixa;
    [SerializeField]
    private Sprite usedCaixa;
    [SerializeField]
    private List<Image> vidas;

    [SerializeField]
    private Button menu;
    [SerializeField]
    private GameObject painel;

    [SerializeField]
    private GameObject painelFimFase;
    [SerializeField]
    private Button btnReiniciarJogo;
    [SerializeField]
    private Button btnAdicionarForca;
    [SerializeField]
    private Button btnAdicionarVelocidade;
    [SerializeField]
    private Text distribuirText;
    [SerializeField]
    private Text forcaText;
    [SerializeField]
    private Text velocidadeText;
    [SerializeField]
    private Button btnMenu;

    [SerializeField]
    private GameObject painelGameOver;
    [SerializeField]
    private Button btnReiniciarGameOver;

    private GameManager _gameManager;

    private int contVelocidade;
    private int contForca;

    // Use this for initialization
    void Start()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        menu.onClick.AddListener(delegate { PresMenu(); });
        btnMenu.onClick.AddListener(delegate { IrMenuInicial(); });
        btnAdicionarForca.onClick.AddListener(delegate { AdicionarForca(); });
        btnAdicionarVelocidade.onClick.AddListener(delegate { AdicionarVelocidade(); });
        btnReiniciarJogo.onClick.AddListener(delegate { Reinicar(); });
        btnReiniciarGameOver.onClick.AddListener(delegate { Reinicar(); });

        contVelocidade = 0;
        contForca = 0;
    }

    public void PresMenu()
    {
        Time.timeScale = 0;
        painel.SetActive(true);
    }

    public void UpdateVida(int numVidas)
    {
        if(numVidas > 3)
        {
            numVidas = 3;
        }
        for(int i = 0; i < vidas.Count; i++)
        {
            if( i < numVidas)
            { 
                vidas[i].sprite = newCaixa;
            } else
            {
                vidas[i].sprite = usedCaixa;
            }
        }
    }

    public void AtualizarPontosParaDistribuir()
    {
        distribuirText.text = "Distribua os pontos: " + _gameManager.pontosParaDistribuir + " pontos restantes";
    }

    public void MostrarTelaFinal()
    {
        Time.timeScale = 0;
        painelFimFase.SetActive(true);
    }

    public void EsconderTelaFinal()
    {
        painelFimFase.SetActive(false);
    }

    public void AdicionarForca()
    {
        if (_gameManager.pontosParaDistribuir > 0)
        {
            _gameManager.pontosParaDistribuir -= 1;
            _gameManager.forca += 0.5f;
            contForca += 1;
            forcaText.text = "FORCA: " + contForca;
            AtualizarPontosParaDistribuir();
        }
    }

    public void AdicionarVelocidade()
    {
        if(_gameManager.pontosParaDistribuir > 0)
        {
            _gameManager.pontosParaDistribuir -= 1;
            _gameManager.velocidade += 0.5f;
            contVelocidade += 1;
            velocidadeText.text = "VELOCIDADE: " + contVelocidade;
            AtualizarPontosParaDistribuir();
            _gameManager.chaoSpawnTime -= 0.5f;
            _gameManager.obsSpawnTime -= -0.5f;
        }
    }

    public void Reinicar()
    {
        painelFimFase.SetActive(false);
        painelGameOver.SetActive(false);
        _gameManager.Reiniciar();
    }

    public void IrMenuInicial()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void MostrarGameOver()
    {
        painelGameOver.SetActive(true);
    }

    public void EsconderGameOver()
    {
        painelGameOver.SetActive(false);
    }
}
