﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    public Button voltar;
    public Button continuar;
    public Button som;
    public Sprite somON;
    public Sprite somOFF;
    public GameObject painel;

    // Use this for initialization
    void Start () {
        voltar.onClick.AddListener(delegate { presVoltar(); });
        som.onClick.AddListener(delegate { presSom(); });
        continuar.onClick.AddListener(delegate { presContinuar(); });
	}
	
	// Update is called once per frame
	void Update () {
        attSom();
	}

    public void presVoltar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void presContinuar()
    {
        painel.SetActive(false);
        Time.timeScale = 1;
    }

    public void presSom()
    {
        if (som.image.sprite == somON)
        {
            som.image.sprite = somOFF;
            AudioListener.pause = true;
        }
        else
        {
            som.image.sprite = somON;
            AudioListener.pause = false;
        }
    }

    public void attSom()
    {
        if (AudioListener.pause == false)
        {
            som.image.sprite = somON;
        } else
        {
            som.image.sprite = somOFF;
        }
    }
}
