﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalFase : MonoBehaviour {

    private float _speed;

    // Use this for initialization
    void Start()
    {
        _speed = 5.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > 0)
        {
            transform.Translate(Vector3.down * _speed * Time.deltaTime);
        }
    }
}
