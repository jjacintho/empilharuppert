﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public int lifes;
    public int lane;

    public float speed;

    private Caixa _caixa;
    private VidaManager _vidaManager;

    private GameManager _gameManager;

    // Use this for initialization
    void Start ()
    {
        speed = 5.0f;
        lifes = 3;
        lane = 0;
        _caixa = GameObject.Find("Caixa").GetComponent<Caixa>();
        _vidaManager = GameObject.Find("Canvas").GetComponent<VidaManager>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        if(_vidaManager != null)
        {
            _vidaManager.UpdateVida(lifes);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (_gameManager.gameOver == false)
        {
            Movement();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                _caixa.ThrowBox();
            }
        }
        else
        {
            AndarAteSaida();
        }
    }

    private void Movement()
    {        
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if(transform.position.x > -3)
            {
                transform.Translate(-3.3f, 0.0f, 0.0f);
                lane--;
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (transform.position.x < 3)
            {
                transform.Translate(3.3f, 0.0f, 0.0f);
                lane++;
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other != null)
        {
            if (other.tag != "Fio")
            {
                Hit();
            }
        }
        if(lifes < 1)
        {
            _vidaManager.MostrarGameOver();
            Time.timeScale = 0;
        }
    }

    public void Hit()
    {
        transform.position = new Vector3(-0.1f, -7.0f, 0.0f);
        lifes--;
        lane = 0;
        _vidaManager.UpdateVida(lifes);
        _caixa.ResetBoxPosition();
    }

    public void AndarAteSaida()
    {
        if (_gameManager.delay == true)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
            _caixa.transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
        if (transform.position.y > 0.35f)
        {
            _vidaManager.MostrarTelaFinal();
        }
    }
}
