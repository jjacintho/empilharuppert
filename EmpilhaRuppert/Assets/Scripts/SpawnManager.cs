﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    [SerializeField]
    private GameObject[] _obstaculos;
    [SerializeField]
    private float[] _posicoes;
    [SerializeField]
    private GameObject _ground;
    [SerializeField]
    private GameObject[] _objetosCenario;
    [SerializeField]
    private GameObject _wall;

    private GameManager _gameManager;
    private int _rndObstaculos;
    private int _rndCenario;

	// Use this for initialization
	void Start ()
    { 
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        StartCoroutines();
	}

    public void StartCoroutines()
    {
        StartCoroutine(ObstaculoRoutine());
        StartCoroutine(GroundRoutine());
        StartCoroutine(CenarioRoutine());
    }

    public IEnumerator ObstaculoRoutine()
    {
        while (_gameManager.gameOver == false)
        {
            _rndObstaculos = Random.Range(0, 3);
            switch (_rndObstaculos)
            {
                case 0:
                    Instantiate(_obstaculos[0], new Vector3(-7.642f, 11f, 0f), Quaternion.identity);
                    yield return new WaitForSeconds(4.0f);
                    break;
                case 1:
                    Instantiate(_obstaculos[1], new Vector3(_posicoes[Random.Range(0, 2)], 11, 0), Quaternion.identity);
                    yield return new WaitForSeconds(4.0f);
                    break;
                case 2:
                    Instantiate(_obstaculos[2], new Vector3(_posicoes[Random.Range(0, 2)], 11, 0), Quaternion.identity);
                    yield return new WaitForSeconds(_gameManager.obsSpawnTime);
                    break;
            }
        }
    }

    public IEnumerator GroundRoutine()
    {
        while (_gameManager.gameOver == false)
        {
            Instantiate(_wall, new Vector3(0, 20, 0), Quaternion.identity);
            Instantiate(_ground, new Vector3(0, 20, 0), Quaternion.identity);
            yield return new WaitForSeconds(_gameManager.chaoSpawnTime);
        }
    }

    public IEnumerator CenarioRoutine()
    {
        while (_gameManager.gameOver == false)
        {
            _rndCenario = Random.Range(0, 4);
            //while (_rndObstaculos != 0) {
                switch (_rndCenario)
                {
                    case 0:
                        Instantiate(_objetosCenario[0], new Vector3(7.6f, 15.5f, 0f), Quaternion.identity);
                        yield return new WaitForSeconds(2.5f);
                        break;
                    case 1:
                        Instantiate(_objetosCenario[1], new Vector3(-7.7f, 15.5f, 0), Quaternion.identity);
                        yield return new WaitForSeconds(2.5f);
                        break;
                    case 2:
                        Instantiate(_objetosCenario[2], new Vector3(8.2f, 14, 0), Quaternion.identity);
                        yield return new WaitForSeconds(2.5f);
                        break;
                    case 3:
                        Instantiate(_objetosCenario[3], new Vector3(-8.5f, 14, 0), Quaternion.identity);
                        yield return new WaitForSeconds(2.5f);
                        break;
                }
            //}
        }
    }
}
