﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caixa : MonoBehaviour {

    private Player _player;
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;
    private GameManager _gameManager;

    public bool flying;
    public int lane;

	// Use this for initialization
	void Start ()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        lane = 0;
        flying = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (_gameManager.gameOver == false)
        {
            if (flying == false)
            {
                Movement();
            }
        }
    }

    private void Movement()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (transform.position.x > -3)
            {
                transform.Translate(-3.3f, 0.0f, 0.0f);
                lane--;
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (transform.position.x < 3)
            {
                transform.Translate(3.3f, 0.0f, 0.0f);
                lane++;
            }
        }
    }

    public void ThrowBox()
    {
        flying = true;
        _animator.SetBool("Arremessou", true);
        _spriteRenderer.sortingOrder = 4;
        StartCoroutine(BoxFlying());
    }

    public void CatchBox()
    {
        if(this.lane != _player.lane)
        {
            _player.Hit();
        }
        _spriteRenderer.sortingOrder = 1;
        _animator.SetBool("Arremessou", false);
        flying = false;
    }

    public void ResetBoxPosition()
    {
        lane = 0;
        transform.position = new Vector3(-0.1f, -5.5f, 0.0f);
    }

    public void mudarVelocidadeAnimacao()
    {
        Animator anim = GetComponent<Animator>();
        anim.speed -= 1;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(flying == false)
        {
            if (other != null)
            {
                _player.Hit();
                
            }
            if (_player.lifes < 1)
            {
                Time.timeScale = 0;
            }
        }
    }

    public IEnumerator BoxFlying()
    {
        yield return new WaitForSeconds(2.0f);
        CatchBox();
    }
}
